import React, { useState, useEffect, Fragment } from 'react';
import Information from './components/Information';
import Form from './components/Form';
import Song from './components/Song';
import axios from 'axios';

function App() {

    const [artist, addArtist] = useState(''); //String
    const [lyric, addLyric] = useState([]); //Array
    const [info, addInfo] = useState({}); //Object


    //Api for request song lyrics
    const requestLyricApi = async(searched) => {
        const {artist, song} = searched;
        const url = `https://api.lyrics.ovh/v1/${artist}/${song}`;
        const result = await axios(url); //Request Api
        addArtist(artist);
        addLyric(result.data.lyrics); //Add lyric to state similar to setState
    };

    //Api for request artist song
    const requestArtistInfoApi = async () => {

        if(artist){
            const url = `https://theaudiodb.com/api/v1/json/1/search.php?s=${artist}`;
            const result = await axios(url);
            addInfo(result.data.artists[0]);
        }
    };

    useEffect(
        () => {
            requestArtistInfoApi();
        }, [artist]
    );


    return (
        <Fragment>
            <Form
                requestLyricApi={requestLyricApi}
            />
            <div className={"container mt-5"}>
                <div className={"row"}>
                    <div className={"col-md-6"}>
                        <Information
                            info={info}
                        />
                    </div>
                    <div className={"col-md-6"}>
                        <Song
                            lyric={lyric}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default App;