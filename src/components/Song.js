import React, {Fragment} from 'react';
 
function Song(props) {
    if(props.lyric.length === 0 ){
        return null;
    }

    return(
        <Fragment>
            <h2>Letra de cancion</h2>
            <p className={"letra"}>{props.lyric}</p>
        </Fragment>
    )
}

export default Song;